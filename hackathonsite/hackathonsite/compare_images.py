import requests

import json

# POST /search
# Searches for a similar image in the database. Scores range from 0 to 100, with 100 being a perfect match.
#
# Parameters
# url or image (required)
#
# The image to add to the database. It may be provided as a URL via url or as a multipart/form-data file upload via image.
#
# all_orientations (default: true)
#
# Whether or not to search for similar 90 degree rotations of the image.


import requests

import json

# POST /add
# Adds an image signature to the database.
#
# Parameters
# url or image (required)
#
# The image to add to the database. It may be provided as a URL via url or as a multipart/form-data file upload via image.
#
# filepath (required)
#
# The path to save the image to in the database. If another image already exists at the given path, it will be overwritten.
#
# metadata (default: None)
#
# An arbitrary JSON object featuring meta data to attach to the image.
#${x.code_string},${x.brand_text},${x.name_text_en},${x.url_en_string},${x["img-720Wx800H_string"]}

def magic(image_name):
    def generateMetaData(product_code, brand, name, product_url, image_url):
        metadata = {}
        metadata['code'] = product_code
        metadata['brand'] = brand
        metadata['name'] = name
        metadata['product_url'] = product_url
        metadata['image_url'] = image_url
        return metadata

    url2_list = []
    url2_list_indexed = ["https://cdn.discounttire.com/sys-master/images/h03/h5e/8828174565406/wheel_lum_morro_silver_mirror-machined_machined-lip_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/he6/hf6/8828100018206/wheel_cov_hornet_silver_mirror-machined_machined-lip_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h6d/h8c/8828232531998/wheel_tsw_panorama_silver_mirror-machined_machined-spokes_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h89/h7e/8828229320734/wheel_tsw_jarama_silver_gloss_mirror-machined-lip_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h46/hf6/8828236038174/wheel_tsw_tanaka_silver_gloss_na_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h2b/h3a/8880777232414/wheel_kon_00544_silver_silver_na_20_zoom.png", "https://cdn.discounttire.com/sys-master/images/h31/h99/8828224077854/wheel_rge_r5_silver_gloss_na_5_20_dp.jpg", "https://cdn.discounttire.com/sys-master/images/hac/hce/8828081766430/wheel_bgr_rodder_silver_gloss_machined-lip_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/hcb/h86/8828266053662/wheel_vox_mga_silver_machined_machined-spokes_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h3b/h5e/8828247801886/wheel_vis_bane_silver_pvd_na_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/ha0/hb9/8828081111070/wheel_bey_rapp_silver_mirror-machined_machined-lip_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h76/h82/8828237316126/wheel_tsw_vortex_silver_gloss_na_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/hca/h6e/8828179546142/wheel_mbm_chaos-6lg_silver_gloss_machined-lip_6_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h3d/hc7/8828176793630/wheel_mbm_14_silver_metallic_na_5_20_dp.jpg", "https://cdn.discounttire.com/sys-master/images/h89/h7e/8828229320734/wheel_tsw_jarama_silver_gloss_mirror-machined-lip_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/he6/hf6/8828100018206/wheel_cov_hornet_silver_mirror-machined_machined-lip_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/hcd/h78/8828164341790/wheel_liq_f5_silver_gloss_machined-lip_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h06/h64/8828075474974/wheel_amr_vn515-torq-thrst-ii_silver_polished_na_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/hcb/h86/8828266053662/wheel_vox_mga_silver_machined_machined-spokes_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h50/h54/8828166176798/wheel_liq_merker_silver_machined_mirror-machined-lip_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/hac/hce/8828081766430/wheel_bgr_rodder_silver_gloss_machined-lip_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/ha5/ha4/8828201828382/wheel_prc_29_silver_machined_na_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h64/ha8/8828201959454/wheel_prc_29_silver_machined_na_8_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h94/h55/8828226863134/wheel_tsw_carthage_silver_mirror-machined_ball-cut-machined_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h3d/hc5/8828162899998/wheel_liq_core_silver_gloss_machined-lip_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h89/h7e/8828229320734/wheel_tsw_jarama_silver_gloss_mirror-machined-lip_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h46/hf6/8828236038174/wheel_tsw_tanaka_silver_gloss_na_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h46/hf6/8828236038174/wheel_tsw_tanaka_silver_gloss_na_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h31/h99/8828224077854/wheel_rge_r5_silver_gloss_na_5_20_dp.jpg", "https://cdn.discounttire.com/sys-master/images/h47/hcf/8828167684126/wheel_liq_static_silver_gloss_na_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h6d/h8c/8828232531998/wheel_tsw_panorama_silver_mirror-machined_machined-spokes_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h47/hcf/8828167684126/wheel_liq_static_silver_gloss_na_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h40/hb8/8828235776030/wheel_tsw_stowe_silver_mirror-machined_machined-lip_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/hea/hcd/8828155822110/wheel_kon_solution_silver_machined_machined-spokes_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h19/he2/8828150743070/wheel_kon_inception_silver_machined_machined-spokes_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h24/ha9/8880998842398/wheel_tsw_01442_silver_silver-w/mirror-cut-face_mirror-machined-lip_20_zoom.png", "https://cdn.discounttire.com/sys-master/images/h68/h87/8880993370142/wheel_tsw_01956_black_gloss-black-w/mirror-cut-face_machined-spokes_20_zoom.png", "https://cdn.discounttire.com/sys-master/images/hbb/h50/8828263694366/wheel_vlx_sidewinder_silver_gloss_machined-lip_4_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h46/hf6/8828236038174/wheel_tsw_tanaka_silver_gloss_na_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h76/h82/8828237316126/wheel_tsw_vortex_silver_gloss_na_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h31/h99/8828224077854/wheel_rge_r5_silver_gloss_na_5_20_dp.jpg", "https://cdn.discounttire.com/sys-master/images/h06/h64/8828075474974/wheel_amr_vn515-torq-thrst-ii_silver_polished_na_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/hcb/h86/8828266053662/wheel_vox_mga_silver_machined_machined-spokes_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/ha0/hb9/8828081111070/wheel_bey_rapp_silver_mirror-machined_machined-lip_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/ha0/hb9/8828081111070/wheel_bey_rapp_silver_mirror-machined_machined-lip_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h50/h54/8828166176798/wheel_liq_merker_silver_machined_mirror-machined-lip_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h47/hc2/8881273765918/wheel_xds_01665_chrome-silver_chrome-plated_na_20_zoom.png", "https://cdn.discounttire.com/sys-master/images/h94/h55/8828226863134/wheel_tsw_carthage_silver_mirror-machined_ball-cut-machined_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h3d/hc7/8828176793630/wheel_mbm_14_silver_metallic_na_5_20_dp.jpg", "https://cdn.discounttire.com/sys-master/images/h76/h82/8828237316126/wheel_tsw_vortex_silver_gloss_na_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/hbe/hbe/8828133802014/wheel_dx4_7s_silver_machined_black-accent_6_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h8c/h36/8828234072094/wheel_tsw_rivage_silver_gloss_na_5_20_dp.png", "https://cdn.discounttire.com/sys-master/images/h96/h80/8828180332574/wheel_mbm_five-x_silver_metallic_machined-lip_5_20_dp.jpg", "https://cdn.discounttire.com/sys-master/images/h79/h5a/8828169715742/wheel_liq_wire_silver_gloss_machined-spokes_5_20_dp.png"]

    with open("/Users/johann/clients/avatria/DT/hackathon/dt-avatria-hackathon/hackathonsite/hackathonsite/product_code_imagelist2.txt", "r") as product_list_file:
        for line in product_list_file:
            line_parts = line.strip().split(',')
            print(line_parts)
            assert len(line_parts) == 5
            product_url = line_parts[3] if line_parts[3].startswith('http') else 'https://www.discounttire.com' + line_parts[3]
            all_data = generateMetaData(line_parts[0],line_parts[1],line_parts[2],product_url,line_parts[4])
            if(all_data['image_url'] in url2_list_indexed):
                url2_list.append(all_data)




    # url1 = 'https://cdn.discounttire.com/sys-master/images/h3b/h5e/8828247801886/wheel_vis_bane_silver_pvd_na_5_20_dp.png'
    #url1 = 'https://cdn.discounttire.com/sys-master/images/h24/ha9/8880998842398/wheel_tsw_01442_silver_silver-w/mirror-cut-face_mirror-machined-lip_20_zoom.png'
    # url1 = 'http://host.docker.internal:8000/static/THE_IMAGE.JPG'
    url1 = 'http://host.docker.internal:8000/static/' + image_name
    print(url2_list_indexed)


    list_with_score = []
    for image_obj in url2_list[0:10]:
        r = requests.post('http://localhost:8888/compare',
          data = {
              'url1': url1,
              'url2': image_obj['image_url']
          }
        )
        image_obj['score'] = r.json()['result'][0]['score']
        list_with_score.append(image_obj)
        print(r)
        print(r.status_code)
        print(r.json()['result'][0]['score'])
    print(list_with_score)
    sorted_list = sorted(list_with_score,key=lambda k: k['score'],reverse=True)
    for x in sorted_list:
        print(x['score'])

    return sorted_list

#magic()
