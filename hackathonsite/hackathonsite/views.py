from django.shortcuts import render
from django.http import HttpResponseRedirect

from hackathonsite import compare_images

from django.shortcuts import render
from django.conf import settings
from django.core.files.storage import FileSystemStorage

def index(request):
    context = {'index': 'index'}
    return render(request, 'hackathonsite/index.html', context)


def wheel_selector_landing(request):
    context = {'wheel_selector': 'wheel_selector'}
    return render(request, 'hackathonsite/wheel_selector.html', context)





def discover(request):
    context = {'discover': 'discover'}
    return render(request, 'hackathonsite/discover.html', context)


def wheel_upload(request):
    context = {'wheel_upload': 'wheel_upload'}
    if request.method == 'POST' and request.FILES['myfile']:
        myfile = request.FILES['myfile']
        fs = FileSystemStorage()
        filename = fs.save("/Users/johann/clients/avatria/DT/hackathon/dt-avatria-hackathon/hackathonsite/hackathonsite/static/images/" + myfile.name, myfile)
        uploaded_file_url = fs.url(filename)
        print(uploaded_file_url)
        print(myfile.name)
        sorted_list = compare_images.magic(myfile.name)
        context = {'matches': sorted_list}
        return render(request, 'hackathonsite/matches.html', context)
    return render(request, 'hackathonsite/wheel_upload.html', context)


def simple_upload(request):
    if request.method == 'POST' and request.FILES['myfile']:
        myfile = request.FILES['myfile']
        fs = FileSystemStorage()
        filename = fs.save(myfile.name, myfile)
        uploaded_file_url = fs.url(filename)
        return render(request, 'core/simple_upload.html', {
            'uploaded_file_url': uploaded_file_url
        })
    return render(request, 'core/simple_upload.html')


def matches(request):
    sorted_list = compare_images.magic()
    context = {'matches': sorted_list}
    return render(request, 'hackathonsite/matches.html', context)






