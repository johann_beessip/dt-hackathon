import requests

import json



# POST /add
# Adds an image signature to the database.
#
# Parameters
# url or image (required)
#
# The image to add to the database. It may be provided as a URL via url or as a multipart/form-data file upload via image.
#
# filepath (required)
#
# The path to save the image to in the database. If another image already exists at the given path, it will be overwritten.
#
# metadata (default: None)
#
# An arbitrary JSON object featuring meta data to attach to the image.
#${x.code_string},${x.brand_text},${x.name_text_en},${x.url_en_string},${x["img-720Wx800H_string"]}

def generateMetaData(product_code, brand, name, product_url):
    metadata = {}
    metadata['code'] = product_code
    metadata['brand'] = brand
    metadata['name'] = name
    metadata['product_url'] = product_url
    return json.dumps(metadata)

def add_to_index(product_code, brand, name, product_url, image_url):
    json_data = generateMetaData(product_code, brand, name, product_url)

    filepath = image_url

    r = requests.post('http://localhost:8888/add',
      data = {
          'url': image_url,
          'filepath':filepath,
          'metadata': json_data
      }
    )

    print(r)
    print(r.status_code)
    print(r.json())



with open("./product_code_imagelist2.txt", "r") as product_list_file:
    for line in product_list_file:
        line_parts = line.strip().split(',')
        print(line_parts)
        assert len(line_parts) == 5
        product_url = line_parts[3] if line_parts[3].startswith('http') else 'https://www.discounttire.com' + line_parts[3]
        print(generateMetaData(line_parts[0],line_parts[1],line_parts[2],product_url))

        add_to_index(line_parts[0],line_parts[1],line_parts[2],product_url,line_parts[4])